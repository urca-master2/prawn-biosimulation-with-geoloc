# Main.py
# Author: EMERY Alexandre
# Date: 31/01/2024

import pygame

from Settings import *

from GUI.Launcher import Launcher
from Simulation.PrawnSimulation import PrawnSimulation

# Initialize Pygame
pygame.init()

# Window settings
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption(APP_NAME)

# Fonts settings
pygame.font.init()
        
# Init interface
launcher = Launcher(screen)
simulation = PrawnSimulation(screen)

# Start App
launcher.start()
simulation.start()

# # Show graph stats
simulation.plot_simulation_data()
