# PrawnSimulation.py
# Author: EMERY Alexandre
# Date: 31/01/2024

# Import necessary libraries
import pygame
import random
import numpy as np

# Import matplotlib for plotting
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from datetime import timedelta

# Import custom modules and classes
from Settings import *
from Entity.Prawn import Prawn
from Utils.Position import Position
from Utils.Color import Color
from Utils.Brightness import calculate_brightness

# Initialize the pygame font and clock
pygame.font.init()
font = pygame.font.Font(None, 24)
clock = pygame.time.Clock()

# Class for simulating a Simulation environment with prawns
class PrawnSimulation:

    def __init__(self, screen):
        """
        Initializes the Prawn Simulation Simulation.

        :param screen: The pygame screen where the simulation will be rendered.
        """
        self._screen = screen
        self._background_texture = pygame.image.load('Assets/Textures/ground_texture.png')
        self._background_texture = pygame.transform.scale(self._background_texture, (SCREEN_WIDTH, SCREEN_HEIGHT))
        self._prawns = [Prawn(Position(random.randint(0, SCREEN_WIDTH - 80), SCREEN_HEIGHT - 80)) for _ in range(NB_PRAWNS)]
        self._simulation_time = 0
        self._simulation_hour = timedelta(hours=6)
        
        self._simulation_times = [] 
        self._prawn_brightness = []
        self._average_brightness = [] 
        
    def update_brightness_based_on_time(self):
        """
        Update the brightness based on the simulation time.

        Returns:
            Tuple[float, float]: The maximum and minimum brightness values.
        """
        total_seconds = self._simulation_hour.total_seconds()
        hour = (total_seconds % 86400) / 3600

        MAX_BRIGHTNESS = 0.7 + 0.3 * (1 - abs((hour % 12) / 6 - 1))

        # Calculate minimum brightness
        MIN_BRIGHTNESS = 0.2 + 0.3 * (1 - abs((hour % 12) / 6 - 1))

        return MAX_BRIGHTNESS, MIN_BRIGHTNESS

    def start(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    return
                
            elapsed_time = clock.tick(TICK) / 1000
            self._simulation_time += elapsed_time
            self._simulation_hour += timedelta(minutes=1)
            
            max_brightness, min_brightness = self.update_brightness_based_on_time()
            
            simulation_time_str = str(self._simulation_hour).split(", ")[-1]
            
            prawn_brightness = [prawn.brightness for prawn in self._prawns]
            average_brightness = np.mean(prawn_brightness)
            self._average_brightness.append(average_brightness)
            
            for prawn in self._prawns:
                prawn.move(min_brightness, max_brightness)
                self._prawn_brightness.append(prawn.brightness)
                    
            # Draw the ground
            self.draw_ground()
            
            # Drawn brightness bar
            self.draw_brightness_bar(min_brightness, max_brightness)
            
            # drawn simulation time
            self.render_text(simulation_time_str, Color.WHITE, (SCREEN_WIDTH - 100, 10))
            
            # Draw the prawns
            for prawn in self._prawns:
                self.draw_prawn(prawn)
                
            self._simulation_times.append(self._simulation_hour.total_seconds())
                            
            pygame.display.flip()
            clock.tick(TICK)
                        
    def render_text(self, text, color, position):
        """
        Renders text on the screen.

        :param text: The text to be rendered.
        :param color: The color of the text.
        :param position: The position where the text will be rendered on the screen.
        """
        text_surface = font.render(text, True, color)
        self._screen.blit(text_surface, position)

    def draw_ground(self):
        """
        Draws the ground texture on the screen. This method is called every frame to ensure the ground 
        is always rendered in the background.
        """
        self._screen.blit(self._background_texture, (0, 0))
        
    def draw_prawn(self, prawn):
        """
        Draws a prawn and its current nectar count on the screen.

        :param prawn: The prawn object to be drawn.
        """
        self._screen.blit(prawn.texture, (prawn.position.x, prawn.position.y))
        
    def draw_brightness_bar(self, min_brightness, max_brightness):
        """
        Draw a brightness bar on the screen.

        Args:
            max_brightness (float): The maximum brightness value.
            min_brightness (float): The minimum brightness value.
        """
        bar_width = 20
        bar_height = SCREEN_HEIGHT - 20
        bar_x = 10
        bar_y = 10

        for i in range(bar_height):
            brightness = calculate_brightness(i, min_brightness, max_brightness)
            
            hue = 0.1 + (brightness * 0.4)
            saturation = 1.0
            value = 1.0
            color = pygame.Color(0)
            color.hsva = (int(hue * 360), int(saturation * 100), int(value * 100), 100)
            
            pygame.draw.line(self._screen, color, (bar_x, bar_y + i), (bar_x + bar_width, bar_y + i))
            
        self.render_text(f"Max : {max_brightness}", Color.WHITE, (bar_x + bar_width + 5, bar_y))

        self.render_text(f"Min : {min_brightness}", Color.WHITE, (bar_x + bar_width + 5, bar_y + bar_height - 20))

        self.render_text("Luminosité", Color.WHITE, (bar_x + bar_width + 5, bar_y + bar_height // 2))

    def plot_simulation_data(self):
        plt.figure(figsize=(12, 6))

        for i in range(NB_PRAWNS):
            plt.plot(self._simulation_times, self._prawn_brightness[i::NB_PRAWNS], label=f"Prawn {i+1}")

        plt.title('Luminosité des Prawns au fil du temps')
        plt.xlabel('Temps (secondes)')
        plt.ylabel('Luminosité')
        plt.legend()
        plt.grid(True)

        plt.tight_layout()
        plt.show()