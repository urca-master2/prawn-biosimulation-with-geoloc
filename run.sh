#!/bin/bash

# Mise à jour de pip
python3 -m pip install --upgrade pip

# Installation des dépendances
pip3 install -r requirements.txt

# Exécution du programme
python3 Main.py