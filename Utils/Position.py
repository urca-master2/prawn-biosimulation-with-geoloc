# Position.py
# Author: EMERY Alexandre
# Date: 31/01/2024

class Position:
    """
    This class represents a two-dimensional position with x and y coordinates.
    """
    
    def __init__(self, x=0, y=0):
        """
        Initializes a new Position instance.

        :param x: The x-coordinate of the position.
        :param y: The y-coordinate of the position.
        """
        self._x = x
        self._y = y

    @property
    def x(self):
        """
        Returns the x-coordinate of the position.
        """
        return self._x

    @x.setter
    def x(self, value):
        """
        Sets the x-coordinate of the position.

        :param value: The new value for the x-coordinate.
        """
        self._x = value
        
    @property
    def y(self):
        """
        Returns the y-coordinate of the position.
        """
        return self._y

    @y.setter
    def y(self, value):
        """
        Sets the y-coordinate of the position.

        :param value: The new value for the y-coordinate.
        """
        self._y = value
        
    def is_close_to(self, other_position, threshold=5):
        """Check if the position is close to another position within a certain threshold."""
        return abs(self._x - other_position.x) <= threshold and abs(self._y - other_position.y) <= threshold
    

    def distance_to(self, other_position):
        """
        Calculates the Euclidean distance to another position.

        :param other_position: The other Position object to calculate the distance to.
        :return: The Euclidean distance between this position and the other position.
        """
        return ((self._x - other_position.x) ** 2 + (self._y - other_position.y) ** 2) ** 0.5

    def normalize(self):
        """
        Normalizes the position vector to a unit vector.
        """
        magnitude = (self._x ** 2 + self._y ** 2) ** 0.5
        if magnitude != 0:
            self._x /= magnitude
            self._y /= magnitude
            
    def add(self, other_position):
        """
        Adds another position to this position.

        :param other_position: The Position object to add.
        :return: A new Position object representing the sum.
        """
        return Position(self._x + other_position.x, self._y + other_position.y)

    def as_tuple(self):
        """
        Returns the position as a tuple.

        :return: Tuple representation of the position (x, y).
        """
        return self._x, self._y
    
    def move_in_direction(self, direction, distance=1):
        """
        Moves the position in the specified direction by a certain distance.

        :param direction: The direction to move in, represented as a tuple (dx, dy).
        :param distance: The distance to move in the specified direction. Default is 1.
        """
        if not isinstance(direction, tuple):
            raise ValueError("Direction must be a tuple representing (dx, dy)")

        dx, dy = direction
        self._x += dx * distance
        self._y += dy * distance
            
    def __eq__(self, other):
        if not isinstance(other, Position):
            # Don't attempt to compare against unrelated types
            return NotImplemented

        return self._x == other._x and self._y == other._y