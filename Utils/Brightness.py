# Brightness.py
# Author: EMERY Alexandre
# Date: 31/01/2024

# Import custom modules and classes
from Settings import *

def calculate_brightness(y_position, MIN_BRIGHTNESS, MAX_BRIGHTNESS):
    height_fraction = y_position / SCREEN_HEIGHT
    brightness = MIN_BRIGHTNESS + (MAX_BRIGHTNESS - MIN_BRIGHTNESS) * (1 - height_fraction)
    return brightness
