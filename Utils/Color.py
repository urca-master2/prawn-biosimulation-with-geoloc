# Color.py
# Author: EMERY Alexandre
# Date: 31/01/2024

class Color:
    WHITE = (255, 255, 255)
    GREEN = (0, 255, 0)
    YELLOW = (255, 255, 0)
    RED = (255, 0, 0)
    BLACK = (0, 0, 0)
    GRAY = (128, 128, 128)