# Prawn.py
# Author: EMERY Alexandre
# Date: 31/01/2024

# Import necessary libraries
import pygame
import numpy as np

# Import custom modules and classes
from Settings import *
from Utils.Position import Position
from Utils.Brightness import calculate_brightness

class Prawn:
    """
    Represents a prawn environment. The prawn can move around.
    """
    
    def __init__(self, position=None):
        """
        Initializes a new Prawn instance.

        :param position: The starting position of the prawn. Defaults to a new Position instance if not provided.
        """
        self._position = Position() if position is None else position
        self._color = (235, 206, 88)
        self._texture = pygame.transform.scale(pygame.image.load('Assets/Textures/prawn_texture.png'), (80, 80))
        self._desired_brightness = DESIRED_BRIGHTNESS
        self._brightness = 0
        
        self.neuron_weights = np.random.rand(9)
        self.learning_rate = LEARNING_RATE
        self.exploration_rate = EXPLORATION_RATE

    @property                     
    def position(self):
        """
        Gets the current position of the prawn.

        :return: The Position object representing the prawn's location.
        """
        return self._position

    @position.setter
    def position(self, value):
        """
        Sets the position of the prawn.

        :param value: The new position for the prawn, must be an instance of Position.
        :raises ValueError: If value is not an instance of Position.
        """
        if isinstance(value, Position):
            self._position = value
        else:
            raise ValueError("value must be a Position instance")
       
    @property 
    def texture(self):
        """
        Getter for the prawn's texture.
        Returns the texture of the prawn.
        """
        return self._texture

    @property  
    def color(self):
        """Returns the color of the prawn."""
        return self._color
    
    @property                     
    def desired_brightness(self):
        """
        Gets the desired brightness.

        :return: The desired brightness
        """
        return self._desired_brightness
    
    @property 
    def brightness(self):
        """
        Gets the current brightness.

        :return: The brightness
        """
        return self._brightness
    
    def choose_direction(self):
        """
        Determines the next direction based on the current weights of the neuron.

        This method computes the probabilities for each direction by normalizing the non-negative weights of the neuron. 
        If all weights are zero, it assigns equal probability to each direction. Otherwise, it calculates the probabilities 
        based on the non-negative weights. It then randomly selects a direction based on these probabilities.

        Returns:
            int: The chosen direction as an integer, where directions are mapped from 0 to 8, corresponding to 
            different movements (including staying in place).
        """
        non_negative_weights = np.maximum(self.neuron_weights, 0)
        total_weight = np.sum(non_negative_weights)
        if total_weight == 0:
            direction_probs = np.ones(len(non_negative_weights)) / len(non_negative_weights)
        else:
            direction_probs = non_negative_weights / (total_weight + 1e-9)
        direction = np.random.choice(range(9), p=direction_probs)
        return direction

    def move(self, min_brightness, max_brightness):
        """
        Moves the entity in a direction chosen by `choose_direction`.

        This method first selects a direction using `choose_direction`, then computes the new position based on this 
        direction. The movement is constrained within the bounds of the screen (0 to SCREEN_WIDTH-40 for x and 
        0 to SCREEN_HEIGHT-40 for y). After moving, it updates the neuron weights according to the change in brightness 
        encountered due to the move.

        The movement is determined by a mapping of direction indices to deltas in the x and y coordinates. The entity 
        moves by 10 units in the chosen direction, with special handling to ensure it does not move outside the screen 
        boundaries.
        """
        direction = self.choose_direction()
        self._brightness = calculate_brightness(self._position.y, min_brightness, max_brightness)
        
        direction_mapping = {
            0: (-1, -1),
            1: (0, -1),
            2: (1, -1),
            3: (-1, 0),
            4: (1, 0),
            5: (-1, 1),
            6: (0, 1),
            7: (1, 1),
            8: (0, 0)
        }
        
        dx, dy = direction_mapping[direction]
        newX = max(0, min(self._position.x + dx * 10, SCREEN_WIDTH - 40))
        newY = max(0, min(self._position.y + dy * 10, SCREEN_HEIGHT - 40))
        self._position.x = newX
        self._position.y = newY
        
        self.update_weights(min_brightness, max_brightness, direction)
        
    def update_weights(self, min_brightness, max_brightness, chosen_direction):
        """
        Updates the neuron weights based on the error between the current and desired brightness.

        This method adjusts the weights of the neuron to guide the entity towards areas of the screen with brightness
        closer to a desired value. It increases the weight of the chosen direction if it leads to a brightness closer to 
        the desired level and decreases the weights of other directions slightly. The adjustments are proportional to the 
        absolute error between the current brightness and the desired brightness, scaled by the learning rate and an 
        adjustment factor to ensure small errors lead to smaller adjustments.

        Parameters:
            chosen_direction (int): The index of the direction chosen by the `choose_direction` method, which corresponds 
            to the direction that was taken.

        The method also ensures that the weights are kept within a range of -1 to 1 to prevent any weight from having an 
        undue influence on the direction selection process.
        """
        current_brightness = calculate_brightness(self._position.y, min_brightness, max_brightness)
        error = self._desired_brightness - current_brightness
        adjustment_factor = abs(error) / (self._desired_brightness + 1e-9)
        
        for i in range(9):
            if i == chosen_direction:
                self.neuron_weights[i] += self.learning_rate * error * adjustment_factor
            else:
                self.neuron_weights[i] -= self.learning_rate * error * adjustment_factor * 0.5
        
        self.neuron_weights = np.clip(self.neuron_weights, -1, 1)