# Launcher.py
# Author: EMERY Alexandre
# Date: 14/01/2024

# Import necessary libraries
import pygame

# Import custom modules and classes
from Settings import *
from Utils.Color import Color

# Initialize the pygame font and clock
pygame.font.init()
font = pygame.font.Font(None, 24)
clock = pygame.time.Clock()

class Launcher:

    def __init__(self, screen):
        """
        Initializes the Launcher instance.

        :param screen: The pygame screen where the launcher and its components will be rendered.
        """
        self._screen = screen
        self._background_texture = pygame.image.load('Assets/Textures/bg_launcher.png')
        
        self._start_button = None
        self._text_surface = None
        self._text_rect = None

    def start(self):
        """
        Starts the launcher. This method initializes the components, handles events, and renders the 
        launcher components on the screen. It returns True if the start button is clicked, indicating 
        that the simulation should be launched, and False if the pygame window is closed.
        """
        self.init_components()

        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    return False
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if self._start_button.collidepoint(event.pos):
                        return True
                    
            self.draw_components()
            
            pygame.display.flip()
            clock.tick(60)
            
    def draw_ground(self):
        """
        Draws the ground texture on the screen. This method is called every frame to ensure the 
        background is always rendered.
        """
        self._screen.blit(self._background_texture, (0, 0))
        
    def init_components(self):
        """
        Initializes the components of the launcher, including the start button and the text surface 
        for the button's label.
        """
        text = "Lancer la Simulation"
        self._text_surface = font.render(text, True, Color.BLACK)
        self._text_rect = self._text_surface.get_rect()

        padding = 30
        button_width = self._text_rect.width + padding
        button_height = self._text_rect.height + padding
        self._start_button = pygame.Rect(SCREEN_WIDTH / 2 - button_width / 2, SCREEN_HEIGHT / 2 - button_height / 2, button_width, button_height)

    def draw_components(self):
        """
        Draws the components of the launcher on the screen. This includes the ground, the start 
        button, button border, shadow effect, and the text label on the button.
        """
        self.draw_ground()
        pygame.draw.rect(self._screen, Color.WHITE, self._start_button)
        
        border_width = 3
        pygame.draw.rect(self._screen, Color.BLACK, self._start_button, border_width)
        
        shadow_offset = 5
        shadow_rect = pygame.Rect(self._start_button.x + shadow_offset, self._start_button.y + shadow_offset, self._start_button.width, self._start_button.height)
        pygame.draw.rect(self._screen, Color.GRAY, shadow_rect)

        text_x = self._start_button.x + (self._start_button.width - self._text_rect.width) / 2
        text_y = self._start_button.y + (self._start_button.height - self._text_rect.height) / 2
        self._screen.blit(self._text_surface, (text_x, text_y))
